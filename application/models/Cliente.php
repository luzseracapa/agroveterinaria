<?php
   class Cliente extends CI_Model
   {
    function __Contructor()
    {
        parent::__constructor();
    }
    ////funcion para insertar instructor de mysql
    function insertar($datos){
        return $this->db->insert("cliente", $datos);
    }

    //funcion para consultar instructores
    function obtenerTodos(){
        $listadoNuevos=$this->db->get("cliente");///llamar o consultar datos
        if($listadoNuevos->num_rows()>0){///validar datos, si hay datos,comprobacion con if y else
            return $listadoNuevos->result();
        }else{//no hay datos
            return false;
        }
    }
    ////borrar instructores
    function borrar($id_cli){
        $this->db->where("id_cli",$id_cli);
        if($this->db->delete("cliente")){
            return true;
        } else{
            return false;
        }
    }
   }//cire de la clase

?>