<?php
    class Nuevos extends CI_Controller
    {
        function __construct()
        {
            parent:: __construct();
            //cargar modelo
            $this->load->model('Cliente');
            $this->load->model('Cita');
        }
        
        //fucion que renderiza la vista index
        public function indexcli()
        {
            $data['nuevos']=$this->Cliente->obtenerTodos();
            $this->load->view('header');
            $this->load->view('nuevos/indexcli', $data);
            $this->load->view('footer');
        }
    
        public function indexci()
        {
            $data['nuevos']=$this->Cita->obtenerTodos2();
            $this->load->view('header');
            $this->load->view('nuevos/indexci', $data);
            $this->load->view('footer');
        }
        //fucion que renderiza la vista nuevo
        public function clientes(){
            $this->load->view('header');
            $this->load->view('nuevos/clientes');
            $this->load->view('footer');
        }
    
        public function citas(){
            $this->load->view('header');
            $this->load->view('nuevos/citas');
            $this->load->view('footer');
        }
        public function galerias(){
            $this->load->view('header');
            $this->load->view('nuevos/galerias');
            $this->load->view('footer');
        }
        ///funcion para guardar datos 
        public function guardar(){
            $datosNuevoCliente=array(
                "cedula_cli"=>$this->input->post('cedula_cli'),
                "nombres_cli"=>$this->input->post('nombres_cli'),
                "apellidos_cli"=>$this->input->post('apellidos_cli'),
                "direccion_cli"=>$this->input->post('direccion_cli'),
                "telefono_cli"=>$this->input->post('telefono_cli'),
                "email_cli"=>$this->input->post('email_cli')
            );
            if($this->Cliente->insertar($datosNuevoCliente)){///!para un error
                redirect('nuevos/indexcli');
            }else{
                echo "<h1>ERROR AL INSERTAR</h1>";///para poner un error
            }
        }
        public function eliminar($id_cli){
            if($this->Cliente->borrar($id_cli)){
                redirect('nuevos/indexcli');
            }else{
                echo "ERROR AL BORRAR:(";
            }
        }
        //////////
        public function guardar2(){
            $datosNuevoCita=array(
                "nombre_cliente_cit"=>$this->input->post('nombre_cliente_cit'),
                "email_cliente_cit"=>$this->input->post('email_cliente_cit'),
                "telefono_cliente_cit"=>$this->input->post('telefono_cliente_cit'),
                "nombre_mascota_cit"=>$this->input->post('nombre_mascota_cit'),
                "descripcion_cit "=>$this->input->post('descripcion_cit')
            );
            if($this->Cita->insertar($datosNuevoCita)){///!para un error
                redirect('nuevos/indexci');
            }else{
                echo "<h1>ERROR AL INSERTAR</h1>";///para poner un error
            }
        }
        ///nueva eliminacion
        public function eliminar2($id_cit){
            if($this->Cita->borrar2($id_cit)){
                redirect('nuevos/indexci');
            }else{
                echo "ERROR AL BORRAR:(";
            }
        }
        
    }////cierre de clase
        
?>
