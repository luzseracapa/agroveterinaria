<?php
    class Nuevos extends CI_Controller
    {
        function __construct()
        {
            parent:: __construct();
            //cargar modelo
            $this->load->model('Producto');
        }
        
        //fucion que renderiza la vista index
        public function indexpro()
        {
            $data['nuevos']=$this->Producto->obtenerTodos1();
            $this->load->view('header');
            $this->load->view('productos/indexpro', $data);
            $this->load->view('footer');
        }
        //fucion que renderiza la vista nuevo
        public function productos(){
            $this->load->view('header');
            $this->load->view('productos/producto');
            $this->load->view('footer');
        }
        ///funcion para guardar datos 

        // nuevo insercion
        public function guardar(){
            $datosNuevoProducto=array(
                "nombre_pro'"=>$this->input->post('nombre_pro'),
                "descripcion_pro"=>$this->input->post('descripcion_pro'),
                "precio_pro"=>$this->input->post('precio_pro')
            );
            if($this->Producto->insertar($datosNuevoProducto)){///!para un error
                redirect('nuevos/indexpro');
            }else{
                echo "<h1>ERROR AL INSERTAR</h1>";///para poner un error
            }
        }
        ///nueva eliminacion
        public function eliminar($id_pro){
            if($this->Producto->borrar($id_pro)){
                redirect('nuevos/indexpro');
            }else{
                echo "ERROR AL BORRAR:(";
            }
        }
       
       
        
    }////cierre de clase
        
?>
