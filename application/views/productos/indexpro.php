<h1><b style="color:red">LISTADO DE PRODUTOS</b></h1>

<br>
<?php if ($nuevos): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DESCRIPCION</th>
                <th>PRECIO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($nuevos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_pro;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_pro;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->descripcion_pro;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->precio_pro;?>
                    </td>
                    
        
                    <!-- para poner un lapz dentro de un forech para poder editar -->
                    <td class="text-center">
                        <a href="#" title="Editar Producto" style="color:black">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/productos/eliminar1/<?php echo $filaTemporal->id_pro;?>" title="Eliminar Producto" 
                        style="color:red">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h1><b style="color:green">No hay productos hoy</b></h1>
    <?php endif; ?>
<br>