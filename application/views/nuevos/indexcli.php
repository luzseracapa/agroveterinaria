<div class="row">
    <div class="col-md-8">
    <h1><b style="color:red">LISTADO DE CLIENTES</b></h1>
    </div>
    <div class="col-md-4"> <br>
        <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Cliente
       </a>
    </div>
</div>

<br>
<?php if ($nuevos): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>CEDULA</th>
                <th>NOMBRES</th>
                <th>APELLIDOS</th>
                <th>DIRECCION</th>
                <th>TELEFONO</th>
                <th>EMAIL</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($nuevos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cedula_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombres_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellidos_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_cli;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->email_cli;?>
                    </td>
                    <!-- para poner un lapz dentro de un forech para poder editar -->
                    <td class="text-center">
                        <a href="#" title="Editar Cliente" style="color:black">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/nuevos/eliminar/<?php echo $filaTemporal->id_cli;?>" title="Eliminar Cliente" 
                        onclick="return confirm('Esta seguro de eliminar al cliente');"
                        style="color:red">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <?php print_r($instructores);?> -->
<?php else: ?>
    <h1><b style="color:green">No hay clientes</b></h1>
    <?php endif; ?>
<br>
<br>