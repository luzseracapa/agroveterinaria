<h1><b style="color:red">NUEVA CITA</b></h1>
<form class=""
action="<?php echo site_url(); ?>/nuevos/guardar2"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del cliente para la cita"
          class="form-control"
          name="nombre_cliente_cit" value="" id="nombre_cliente_cit">
      </div>
      <div class="col-md-4">
          <label for="">Email cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo de cliente para cita"
          class="form-control"
          name="email_cliente_cli" value="" id="email_cliente_cli">
      </div>
      <div class="col-md-4">
        <label for="">Telefono cliente:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono de cliente para cita"
        class="form-control"
        name="telefono_cliente_cit" value="" id="telefono_cliente_cit">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Nombre mascota:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre de la mascota para cita"
          class="form-control"
          name="nombre_mascota_cit" value="" id="nombre_mascota_cit">
      </div>
      <div class="col-md-6">
          <label for="">Descripcion:</label>
          <br>
          <input type="text"
          placeholder="Ingrese un mensaje para la cita"
          class="form-control"
          name="descripcion_cit" value="" id="descrpcion_cit">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/nuevos/indexci" class="btn btn-danger">Cancelar
            </a>
        </div>
    </div>
</form>
<br>
<br>


