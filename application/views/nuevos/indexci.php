<div class="row">
    <div class="col-md-8">
    <h1><b style="color:red">LISTADO DE CITAS</b></h1>
    </div>
    <div class="col-md-4"> <br>
        <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Cita
       </a>
    </div>
</div>


<br>
<?php if ($nuevos): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE CLIENTE</th>
                <th>CORREO CLIENTE</th>
                <th>TELEFONO CLIENTE</th>
                <th>NOMBRE MASCOTA</th>
                <th>DESCRIPCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($nuevos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_cit;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_cliente_cit;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->email_cliente_cit;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_cliente_cit;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_mascota_cit;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->descripcion_cit;?>
                    </td>

                    <!-- para poner un lapz dentro de un forech para poder editar -->
                    <td class="text-center">
                        <a href="#" title="Editar Cita" style="color:black">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/nuevos/eliminar2/<?php echo $filaTemporal->id_cit;?>" title="Eliminar Cita" 
                        onclick="return confirm('Esta seguro de eliminar al cita');"
                        style="color:red">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php else: ?>
    <h1><b style="color:green">No hay citas </b></h1>
    <?php endif; ?>
    <br>
    <br>