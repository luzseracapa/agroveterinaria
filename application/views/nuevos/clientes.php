<h1><b style="color:red">NUEVO CLIENTE</b></h1>
<form class=""
action="<?php echo site_url(); ?>/nuevos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_cli" value="" id="cedula_cli">
      </div>
      <div class="col-md-4">
          <label for="">Nombres:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          name="nombres_cli" value="" id="nombres_cli">
      </div>
      <div class="col-md-4">
        <label for="">Apellidos:</label>
        <br>
        <input type="text"
        placeholder="Ingrese los apellidos"
        class="form-control"
        name="apellidos_cli" value="" id="apellidos_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Telefono:</label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_cli" value="" id="telefono_cli">
      </div>
      <div class="col-md-4">
          <label for="">Direccion:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_cli" value="" id="direccion_cli">
      </div>
      <div class="col-md-4">
        <label for="">Email:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el correo"
        class="form-control"
        name="email_cli" value="" id="email_cli">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/nuevos/indexcli" class="btn btn-danger">Cancelar
            </a>
        </div>
    </div>
</form>
<br>
<br>


