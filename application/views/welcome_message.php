<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/fo3.jpg" alt="Bienvenidos" width="100%" height="50%">>
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/fo2.jpg" alt="Animales" width="100%" height="50%">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/fot1.jpg" alt="Plantas" width="100%" height="50%">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<div class="row"
    style="background-color:#ccd1d1; color: black;padding: 20px;">
      <div class="col-md-4 text-center">
            <h4>
              <b style="color:red"> QUIENES SOMOS</b>
            </h4>
            <p>Somos una Familia apasionada en veterinaria para dar 
              buen servicio, a los clientes de mejor manera.
            </p>
      </div>
      <div class="col-md-4 text-center">
        <h4>
            <b style="color:red"> MISION</b>
        </h4>
        <p>Ofrecer un servicio de calidad, que satisfagan los requerimientos  de nuestros 
          clientes; para formar un ambiente de lealtad y confianza, manteniendo una calidad 
          de atención alta.
        </p>
      </div>
      <div class="col-md-4 text-center">
        <h4>
            <b style="color:red"> VISION</b>
        </h4>
        <p>Ser la Veterinaria, con mayor calidad de servicio y variedad de productos 
          en la zona, que satisfagan las necesidades de los clientes.
        </p>
      </div>
   </div>
<br>
<br>